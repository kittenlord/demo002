package kittenlord.demo002;

/**
 * Not sure how to make an automatic test for sound, so here is a manual one. After running the main method, you should
 * hear three sound effects.
 */
public class ManualSoundTest {

	public static void main(String[] args) {
		try (Sound sound = new Sound()) {
			sound.open();
			Thread.sleep(1000);
			sound.play();
			Thread.sleep(1000);
			sound.play();
			Thread.sleep(1000);
			sound.play();
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			throw new IllegalStateException("This should never happen");
		}
	}

}
