package kittenlord.demo002;

import static org.assertj.core.api.Assertions.*;

import java.util.Random;

import org.junit.jupiter.api.Test;

class GameStateTest {

	@Test
	void testInBubble() {
		var state = new GameState();
		var initResult = state.initialize(1000, 1000, 0, 0);
		assertThat(initResult).isTrue();
		state.placeBubbleTo(500, 500);
		assertThat(state.inBubble(500, 500)).isTrue();
		assertThat(state.inBubble(500 + GameState.RADIUS_START, 500)).isTrue();
		assertThat(state.inBubble(500 + GameState.RADIUS_START + 1, 500)).isFalse();
		assertThat(state.inBubble(500 - GameState.RADIUS_START, 500)).isTrue();
		assertThat(state.inBubble(500 - GameState.RADIUS_START - 1, 500)).isFalse();
		assertThat(state.inBubble(500, 500 + GameState.RADIUS_START)).isTrue();
		assertThat(state.inBubble(500, 500 + GameState.RADIUS_START + 1)).isFalse();
		assertThat(state.inBubble(500, 500 - GameState.RADIUS_START)).isTrue();
		assertThat(state.inBubble(500, 500 - GameState.RADIUS_START - 1)).isFalse();
	}

	/**
	 * Verify that the randomly placed first bubble always avoids the provided coordinates.
	 */
	@Test
	void testInitialize() {
		var random = new Random();
		var state = new GameState();
		for (int i = 0; i < 100; i++) {
			int avoidX = random.nextInt(500);
			int avoidY = random.nextInt(500);
			var initResult = state.initialize(500, 500, avoidX, avoidY);
			assertThat(initResult).isTrue();
			assertThat(state.inBubble(avoidX, avoidY)).isFalse();
		}
	}

	@Test
	void testInitializeTooSmall() {
		assertThat(new GameState().initialize(10, 10, 0, 0)).isFalse();
	}

}
