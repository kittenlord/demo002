package kittenlord.demo002;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.util.Random;

import javax.swing.SwingUtilities;

/**
 * State of the game: the position and size of the bubble, the number of bubbles already popped. Also the size of
 * canvas, needed to generate bubbles.
 * <p>
 * In ideal case, the size of canvas would not be a part of the game state, because it can be changed if the window
 * resizes. But that would require checking whether the bubble is still within the visible part of the canvas, etc.
 * Let's keep it simple and assume that the canvas size remains constant during the game.
 */
public class GameState {

	static final int RADIUS_START = 200;
	static final int RADIUS_MIN = 10;

	private Random random = new Random();

	private int canvasWidth;
	private int canvasHeight;
	private int bubbleX = 0;
	private int bubbleY = 0;
	private int bubbleRadius = 0;

	private int score = 0;

	/**
	 * Returns true if the game state was successfully initialized; false if we need to abort the game. The canvas
	 * should be at least twice as large as the bubble, so that we can always generate a new bubble away from the
	 * current mouse cursor position. On the other hand, the bubble should not be too small; which imposes a limit on
	 * the canvas size.
	 */
	public boolean initialize(Canvas canvas) {
		var mouse = MouseInfo.getPointerInfo().getLocation();
		SwingUtilities.convertPointFromScreen(mouse, canvas);
		return initialize(canvas.getWidth(), canvas.getHeight(), mouse.x, mouse.y);
	}

	boolean initialize(int canvasWidth, int canvasHeight, int avoidX, int avoidY) {
		this.canvasWidth = canvasWidth;
		this.canvasHeight = canvasHeight;
		bubbleRadius = RADIUS_START;
		bubbleRadius = Math.min(bubbleRadius, canvasWidth / 4);
		bubbleRadius = Math.min(bubbleRadius, canvasHeight / 4);
		if (bubbleRadius < RADIUS_MIN) {
			return false;
		}
		placeBubbleAvoiding(avoidX, avoidY);
		return true;
	}

	public void drawTo(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, canvasWidth, canvasHeight);
		if (bubbleRadius > 0) {
			g.setColor(Color.CYAN);
			g.fillOval(bubbleX - bubbleRadius, bubbleY - bubbleRadius, 2 * bubbleRadius, 2 * bubbleRadius);
			g.setColor(Color.BLUE);
			g.drawOval(bubbleX - bubbleRadius, bubbleY - bubbleRadius, 2 * bubbleRadius, 2 * bubbleRadius);
		}
	}

	/**
	 * Returns true if the bubble was hit, shrink it and move it to a new position; return false otherwise.
	 */
	public boolean tryCollide(int x, int y) {
		if (!inBubble(x, y)) {
			return false;
		}
		score++;
		// reduce the bubble radius by 5%, but not below the minimum
		bubbleRadius = Math.max((bubbleRadius * 19) / 20, RADIUS_MIN);
		placeBubbleAvoiding(x, y);
		return true;
	}

	/**
	 * How many bubbles were already popped.
	 */
	public int getScore() {
		return score;
	}

	/**
	 * Returns true if the provided coordinates are within the bubble; false otherwise.
	 */
	boolean inBubble(int x, int y) {
		int dx = bubbleX - x;
		int dy = bubbleY - y;
		return dx * dx + dy * dy <= bubbleRadius * bubbleRadius;
	}

	/**
	 * Creates a new bubble at given position.
	 */
	void placeBubbleTo(int x, int y) {
		bubbleX = x;
		bubbleY = y;
	}

	/**
	 * Creates a new bubble at a random position.
	 */
	private void placeBubble() {
		placeBubbleTo( //
				bubbleRadius + random.nextInt(canvasWidth - 2 * bubbleRadius),
				bubbleRadius + random.nextInt(canvasHeight - 2 * bubbleRadius));
	}

	/**
	 * Creates a new bubble at a random position away from the given point.
	 */
	private void placeBubbleAvoiding(int x, int y) {
		do {
			placeBubble();
		} while (inBubble(x, y));
	}

}
