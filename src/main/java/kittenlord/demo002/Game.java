package kittenlord.demo002;

import java.awt.Canvas;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * A bubble-popping game. The player pops randomly placed bubbles by mouse cursor.
 */
public class Game implements Runnable {

	private static final String TITLE = "Bubbles";
	
	private GameState state = new GameState();
	private Sound sound = new Sound();
	private JFrame frame;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Game());
	}

	/**
	 * Creates the game window.
	 */
	@Override
	public void run() {
		var canvas = createCanvas();
		frame = new JFrame(TITLE);
		frame.setIconImage(Resources.getApplicationIcon());
		frame.add(canvas);
		frame.setExtendedState(Frame.MAXIMIZED_BOTH);
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
		frame.addWindowListener(new WindowAdapter() {

			/**
			 * The actual dimensions of the canvas within the maximized game window are only known after the window has
			 * opened.
			 */
			@Override
			public void windowOpened(WindowEvent e) {
				if (state.initialize(canvas)) {
					canvas.repaint();
					sound.open();
				} else {
					JOptionPane.showMessageDialog(frame, "The window is too small", "Error", JOptionPane.ERROR_MESSAGE);
					frame.dispose();
				}
			}

			/**
			 * Release all sound resources after the game window is closed.
			 */
			@Override
			public void windowClosed(WindowEvent e) {
				sound.close();
			}

		});
		canvas.requestFocusInWindow();
	}

	private Canvas createCanvas() {
		@SuppressWarnings("serial")
		var canvas = new Canvas() {

			@Override
			public void update(Graphics g) {
				paint(g);
			}

			@Override
			public void paint(Graphics g) {
				state.drawTo(g);
			}

		};
		canvas.addMouseMotionListener(new MouseAdapter() {

			@Override
			public void mouseMoved(MouseEvent e) {
				if (state.tryCollide(e.getX(), e.getY())) {
					e.consume();
					frame.setTitle(TITLE + " (score: " + state.getScore() + ")");
					canvas.repaint();
					sound.play();
				}
				super.mouseMoved(e);
			}

		});
		return canvas;
	}

}
