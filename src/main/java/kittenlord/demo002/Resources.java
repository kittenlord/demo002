package kittenlord.demo002;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class Resources {

	private static final String APPLICATION_ICON_FILE_NAME = "app.png";
	private static final String BUBBLE_SOUND_FILE_NAME = "bubble.wav";

	public static BufferedImage getApplicationIcon() {
		return getImage(APPLICATION_ICON_FILE_NAME);
	}

	private static BufferedImage getImage(String imageFileName) {
		try (var is = Resources.class.getResourceAsStream(imageFileName)) {
			if (null == is) {
				throw new IllegalStateException(String.format("Resource '%s' not found", imageFileName));
			}
			return ImageIO.read(is);
		} catch (IOException e) {
			throw new IllegalStateException(String.format("Error loading resource '%s'", imageFileName), e);
		}
	}

	/**
	 * Returns a short sound effect.
	 * 
	 * @see https://sfxr.me/
	 */
	public static InputStream getBubbleSoundInputStream() {
		return Resources.class.getResourceAsStream(BUBBLE_SOUND_FILE_NAME);
	}

}
