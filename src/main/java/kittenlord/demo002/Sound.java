package kittenlord.demo002;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Support for playing a sound effect. Because the sound effect is not a critical part of the game, all errors are
 * silently ignored.
 */
public class Sound implements Closeable {

	private InputStream inputStream;
	private AudioInputStream audioInputStream;
	private Clip clip;
	private boolean ready = false;

	/**
	 * Prepares the resources, if possible. Call this method before playing the first sound effect, to prevent it from
	 * being delayed by the audio data still loading.
	 */
	public void open() {
		inputStream = Resources.getBubbleSoundInputStream();
		try {
			audioInputStream = AudioSystem.getAudioInputStream(inputStream);
			clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			ready = true;
		} catch (IOException e) {
			// error processing audio data
		} catch (UnsupportedAudioFileException e) {
			// audio file format not supported
		} catch (LineUnavailableException e) {
			// audio line not available
		}
	}

	/**
	 * Starts playing the sound effect.
	 */
	public void play() {
		if (!ready) {
			return;
		}
		clip.setFramePosition(0);
		clip.start();
	}

	/**
	 * Releases all resources.
	 */
	@Override
	public void close() {
		ready = false;
		if (null != clip) {
			clip.close();
		}
		clip = null;
		if (null != audioInputStream) {
			try {
				audioInputStream.close();
			} catch (IOException e) {
				// ignore
			}
			audioInputStream = null;
		}
		if (null != inputStream) {
			try {
				inputStream.close();
			} catch (IOException e) {
				// ignore
			}
			inputStream = null;
		}
	}

}
